#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"

class Pentagono : public FormaGeometrica{

    private:
        float lado;

    public:
        Pentagono();
        Pentagono(float lado);
        ~Pentagono();
        void set_lado(float lado);
        float get_lado();
        float calcula_area();
        float calcula_perimetro();

};

#endif