#include "circulo.hpp"

Circulo::Circulo(){
    raio = 2.0;
}

Circulo::Circulo(float raio){
    if(raio < 0)
        throw(1);
    set_raio(raio);
    set_tipo("Circulo");
}

Circulo::~Circulo(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

void Circulo::set_raio(float raio){
    this->raio = raio;
}

float Circulo::get_raio(){
    return raio;
}

float Circulo::calcula_area(){
    return 3.14 * pow(raio,2);
}

float Circulo::calcula_perimetro(){
    return 2 * 3.14 * raio;
}