#include "pentagono.hpp"

Pentagono::Pentagono(){
    set_tipo("Pentagono");
    lado = 5.0;
}

Pentagono::Pentagono(float lado){
    set_tipo("Pentagono");
    this->lado = lado;
}

Pentagono::~Pentagono(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

void Pentagono::set_lado(float lado){
    if(lado < 0)
        throw(1);
    this->lado = lado;
}

float Pentagono::get_lado(){
    return lado;
}

float Pentagono::calcula_area(){
    return 1.720477401 * pow(lado, 2);
}

float Pentagono::calcula_perimetro(){
    return 5 * lado;
}