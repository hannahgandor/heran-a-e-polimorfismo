#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

int main(){

    vector < FormaGeometrica * > lista_de_formas;
    lista_de_formas.push_back(new FormaGeometrica(1.0, 2.0));
    lista_de_formas.push_back(new Triangulo(1.0, 2.0));
    lista_de_formas.push_back(new Quadrado(2.0, 2.0));
    lista_de_formas.push_back(new Circulo(4.0));
    lista_de_formas.push_back(new Paralelogramo(6.0, 4.0, 5.0));
    lista_de_formas.push_back(new Pentagono(4.0));
    lista_de_formas.push_back(new Hexagono(4.0));

    cout << endl;
    for(FormaGeometrica *f: lista_de_formas){
        cout << "  Tipo: " << f->get_tipo() << endl;
        cout << "  Área: " << f->calcula_area() << "[cm²]" << endl;
        cout << "  Perimetro: " << f->calcula_perimetro() << "[cm]" << endl;
        cout << endl;
    }

    return 0;
}