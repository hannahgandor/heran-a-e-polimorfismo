#include "hexagono.hpp"

Hexagono::Hexagono(){
    set_tipo("Hexagono");
    lado = 5.0;
}

Hexagono::Hexagono(float lado){
    set_tipo("Hexagono");
    this->lado = lado;
}

Hexagono::~Hexagono(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

void Hexagono::set_lado(float lado){
    if(lado < 0)
        throw(1);
    this->lado = lado;
}

float Hexagono::get_lado(){
    return lado;
}

float Hexagono::calcula_area(){
    return 2.598076211 * pow(lado, 2);
}

float Hexagono::calcula_perimetro(){
    return 6 * lado;
}