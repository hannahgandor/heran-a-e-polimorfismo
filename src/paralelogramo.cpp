#include "paralelogramo.hpp"

Paralelogramo::Paralelogramo(){
    set_tipo("Paralelogramo");
    set_altura(5.5);
    set_base(3.5);
    lado = 4.0;
}

Paralelogramo::Paralelogramo(float altura, float base, float lado){
    set_tipo("Paralelogramo");
    set_altura(altura);
    set_base(base);
    this->lado = lado;
}

Paralelogramo::~Paralelogramo(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

void Paralelogramo::set_lado(float lado){
    if(lado < 0)
        throw(1);
    else
        this->lado = lado;
}

float Paralelogramo::get_lado(){
    return lado;
}

float Paralelogramo::calcula_perimetro(){
    return 2 * (lado + get_base());
}